package com.company;

/**
 * Created by panteyenko on 12/14/2016.
 */
public class Lion extends Cat {
    public double roar = 0.05;

    public Lion(String name, double weight, int height, double roar) {
        super(name, weight, height);
        this.roar = 0.05;
    }

    @Override
    public double getPowerFactor() {
        double temp = super.getPowerFactor();
        double lionPower = (temp*roar) + temp;
        //lionPower=5;
        System.out.println("Lion power = " + Lion.this.name + " " + lionPower);
        return lionPower;
    //    return (super.getPowerFactor() * roar) + super.getPowerFactor();
    }
}