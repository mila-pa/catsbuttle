package com.company;

public class JavaExampleI {

    static Cat[] cats = new Cat[4];

    public static void main(String[] args) {

        //create cats
        cats[0] = new Cat("Chip", 7, 50);
        cats[1]  = new Cat("Godzilla", 9.8, 60);
        cats[2]  = new Lion ("King", 15, 70, 5);
        cats[3]  = new Lion ("Baby", 11, 13, 2);

/*
String[] sa = new String[]{"111", "222", "333", "444", "555"};
int n = (int)Math.floor(Math.random() * sa.length);
System.out.println(sa[n]);
start fighting
*/
int n = 0;
int k = 0;
        while (k == n){
            n = (int) Math.floor(Math.random() * cats.length);
            k = (int) Math.floor(Math.random() * cats.length);
        }
        System.out.printf("%s vs %s", cats[n].name, cats[k].name).println();

        //get winner
        String winnerName = getWinner(cats[n], cats[k]);

        //print winner name
        System.out.printf("%s wins", winnerName);
    }

    /**
     * This method compare to cats and return winner
     * @param catInRedCorner - first cat
     * @param catInBlackCorner - second cat
     * @return winner
     */
    static String getWinner(Cat catInRedCorner, Cat catInBlackCorner) {
        if (catInBlackCorner.getPowerFactor() > catInRedCorner.getPowerFactor()) {
            return catInBlackCorner.name;
        } else {
            return catInRedCorner.name;
        }
    }


}
